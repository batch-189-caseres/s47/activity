// alert("Hello Master")
// querySelector() is a method that can be used to select a specific element from our document
// console.log(document.querySelector("#txt-first-name"));
// document refers to the whole page
// console.log(document);
// document.querySelector("#txt-last-name");

/*
	Alternative that we can use aside from the querySelector in retreiving elements

	document.getElementById("txt-first-name");
	document.getElementsByClassName()
	document.getElementByTagname()

*/

const txtFirstName = document.querySelector("#txt-first-name");
const txtLastName = document.querySelector("#txt-last-name")
const spanFullName = document.querySelector("#span-full-name");


function printFirstName(event) {
	spanFullName.innerHTML = `<h1>${txtFirstName.value} ${txtLastName.value}</h1>`
}
txtFirstName.addEventListener('keyup', printFirstName)

function printLastName(event) {
	spanFullName.innerHTML = `<h1>${txtFirstName.value} ${txtLastName.value}</h1>`
}
txtLastName.addEventListener('keyup', printLastName)





/*
	Event:
		click, hover, keypress and many other things

		Event Listeners
			Allows us to let our users interact with our page. Each click or hover is an event which can trigger 
			a function or a task


	Syntax:
		selectedElement.addEventListener('event', function);
*/

/*txtFirstName.addEventListener('keyup', (event) => {

	spanFullName.innerHTML = `<h1>${txtFirstName.value}</h1>`
});

txtFirstName.addEventListener('keyup', (event) => {

	console.log(event)
	console.log(event.target)
	console.log(event.target.value)
})

const labelFirstName = document.querySelector("#label-txt-name")

labelFirstName.addEventListener('click', (e) => {
	console.log(e)
	alert("You clicked first name label.")
})*/




/*

 innerHTML - is a property of an element which considers all the children of the selected element as a string.

.value - value of the input text field 

*/

